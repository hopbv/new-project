$(document).ready(function(){
    $(".filter").click(function() {
        // $(".list-filter").find(".filter").removeClass("filter-active");
        // $(this).addClass("filter-active");
        // var data_filter = $(this).children().attr("class");
        // $(".list-items").find("*").removeClass("active");
        // $(".list-items").find("."+data_filter).addClass("active");
        $(".list-filter").find(".filter").removeClass("filter-active");
        $(this).addClass("filter-active");
        var data_filter = $(this).children().attr("class");
        $(".list-items").find("*").addClass("active");
        $(".img-container").each(function() {
            if ($(this).hasClass(data_filter) == false) {
                $(this).fadeOut(1500);
                $(this).removeClass("active");
            }
            else {
                $(this).fadeIn(2000);
                $(this).addClass("active");
            }
        });
    });
    $(".img-container").click(function() {
        var src = $(this).children().attr("src");
        $(".modal-body").children().attr("src", src);
    });
});
